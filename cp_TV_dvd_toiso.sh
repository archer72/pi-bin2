#!/bin/bash

echo "What Series is this?"
read series
echo "Series: $series"
echo "What Season is this?"
read season
echo "Season: $season"
echo "Which disc # is this?"
read disknum
echo "This is disk #$disknum"

#mkisofs -dvd-video -o mydvd.iso -V "DVD_TITLE" mydvd
#"$series"_D"$disknum"_E"$track

dvdbackup -M -n "$series"_S"$season"_D"$disknum"
mkisofs -dvd-video -o "$series"_S"$season"_D"$disknum.iso" -V "$series"_S"$season"_"$disknum" "$series"_S"$season"_D"$disknum"
