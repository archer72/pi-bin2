#!/bin/bash
# ~/Bin/makemkv_rip_longest.sh

# Get the minimum length. This ensures that only the longest title goes to MKV.
long_title=$(makemkvcon -r info disc:0 | grep cell | awk '{ print $7 }' | tr -d ')","Title' | sort -nr | head -n1 | awk -F: '{print ($1 * 3600) + ($2 * 60) + $3}')

# show lenth in seconds
echo "length=${long_title}"
