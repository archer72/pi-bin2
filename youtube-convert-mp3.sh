#!/bin/bash
## Extract mp3 audio from mkv mp4 and webm video
for FILE in *.webm;
	## add this to account for multiple extensions
	#extension="${FILE##*.}"; 
	#do ffmpeg -i "$FILE" -f flac -ab 192000 "`basename "$FILE" .$extension`.flac" || break; done
do ffmpeg -i "$FILE" -f mp3 mp3/"`basename "$FILE" `.mp3" || break; done
#clear
#if [ ! -d "flac" ]
#then
#  mkdir tmp
#  echo 'Making flac directory'
#else
#  echo 'tmp directory exists, continuing.' 
#fi
### Remove extraneous characters in the file name
##for FILE in tmp/*.flac; do mv "$FILE" flac"${FILE%%????????????????}.flac"; done
#
echo 'New flac file(s) are in mp3/'

