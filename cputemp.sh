#!/bin/sh

# Script: my-pi-temp.sh # Purpose: Display the ARM CPU and GPU  temperature of Raspberry Pi 2/3 # Author: Vivek Gite <www.cyberciti.biz> under GPL v2.x+ # -------------------------------------------------------                          
#sysctl dev.cpu | grep temperature

#cpu=$(</sys/class/thermal/thermal_zone0/temp)
#echo "$((cpu/1000)) c"

/opt/vc/bin/vcgencmd measure_temp
