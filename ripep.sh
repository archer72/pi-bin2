#!/bin/bash
#read $ISO
echo "Which track to rip?"
read track
echo "track = #$track"
echo "What Series is this?"
read series
echo "Series: $series"
echo "Which disc # is this?"
read disknum
echo "This is disk #$disknum"

# Many of the examples above use: dvdnav://1
 
# You may have better luck with: dvd://1
 
# I was able to rip all the movies I had using.

mplayer dvd://$track -dvd-device /dev/sr0 -dumpstream -dumpfile `pwd`"/"$series"_D"$disknum"_E"$track.mpg
