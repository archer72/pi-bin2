#!/bin/bash

cdrwtool -i -d /dev/cdrom | awk '$1 == "free_blocks" {print $3 * 2048}'
