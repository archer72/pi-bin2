#!/bin/bash

echo "Record name"
read record
echo "Which side is this?"
read side

arecord --device='hw:CARD=CODEC,DEV=0' --rate=96000 --channels=2 --vumeter=stereo --duration=1500 -f dat -t wav $record"_Side_"$side.wav
