#!/bin/bash
echo "Which movie is this?"
read movie
# Many of the examples above use: dvdnav://1
 
# You may have better luck with: dvd://1
 
# I was able to rip all the movies I had using.

tr=1
for track in {1..99}
do
mplayer dvd://$tr -dvd-device /dev/sr0 -alang en -dumpstream -dumpfile $movie"_tr"$tr.mpg
tr=$((tr+1))
done
